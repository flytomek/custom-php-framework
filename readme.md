# Custom PHP framework

## Usage:
`php -S localhost:8000 public/index.php`

### Testing:
`./framework/vendor/bin/phpunit ./framework/tests --colors`


### PHPStan static analysis:
`./framework/vendor/bin/phpstan analyse`
