<?php

declare(strict_types=1);

use Doctrine\DBAL\Connection;
use Flytomek\Framework\Console\Application;
use Flytomek\Framework\Controller\AbstractController;
use Flytomek\Framework\Dbal\ConnectionFactory;
use Flytomek\Framework\Http\Kernel;
use Flytomek\Framework\Routing\Router;
use Flytomek\Framework\Routing\RouterInterface;
use League\Container\Argument\Literal\ArrayArgument;
use League\Container\Argument\Literal\StringArgument;
use League\Container\Container;
use League\Container\ReflectionContainer;
use Symfony\Component\Dotenv\Dotenv;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;


$dotenv = new Dotenv();

$dotenv->load(BASE_PATH. '/.env');

$container = new Container();

$container->delegate(new ReflectionContainer(true));

#parameters

$routes = include BASE_PATH . '/routes/web.php';

$appEnv = $_SERVER['APP_ENV'];
$templatesPath = BASE_PATH . '/templates';

$container->add('APP_ENV', new StringArgument($appEnv));

$databaseUrl = 'sqlite:///' . BASE_PATH . '/var/db.sqlite';

$container->add('base-commands-namespace', new StringArgument('Flytomek\\Framework\\Console\\Command\\'));

# services

$container->add(
    RouterInterface::class,
    Router::class
);

$container->extend(RouterInterface::class)
    ->addMethodCall(
        'setRoutes',
        [new ArrayArgument($routes)]
    );

$container->add(Kernel::class)
    ->addArgument(RouterInterface::class)
    ->addArgument($container);

$container->addShared('filesystem-loader', FilesystemLoader::class)
    ->addArgument(new StringArgument($templatesPath));

$container->addShared('twig', Environment::class)
    ->addArgument('filesystem-loader');

$container->add(AbstractController::class);

$container->inflector(AbstractController::class)
    ->invokeMethod('setContainer', [$container]);

$container->add(ConnectionFactory::class)
    ->addArguments([
        new StringArgument($databaseUrl)
    ]);

$container->addShared(Connection::class, function () use ($container): Connection {
    return $container->get(ConnectionFactory::class)->create();
});

$container->add(\Flytomek\Framework\Console\Kernel::class)
    ->addArguments([$container, Application::class]);

$container->add(Application::class)
    ->addArgument($container);

return $container;
