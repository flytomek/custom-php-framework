<?php

declare(strict_types=1);

namespace Flytomek\Framework\Tests;

use Flytomek\Framework\Container\Container;
use Flytomek\Framework\Container\ContainerException;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    private const DEPENDENT_CLASS_NAME = '\Flytomek\Framework\Tests\DependentClass';

    public function testServiceCanBeRetrievedFromContainerByConcrete(): void
    {
        $container = new Container();

        $container->add('dependent-class', DependentClass::class);

        $this->assertInstanceOf(DependentClass::class, $container->get('dependent-class'));
    }

    public function testServiceCanBeRetrievedFromContainerById(): void
    {
        $container = new Container();

        $container->add(self::DEPENDENT_CLASS_NAME);

        $this->assertInstanceOf(DependentClass::class, $container->get(self::DEPENDENT_CLASS_NAME));
    }

    public function testContainerExceptionIsThrownIfServiceCantBeFound(): void
    {
        $container = new Container();

        $this->expectException(ContainerException::class);

        $container->add('Foo');
    }

    public function testServiceContainerHasParticularService(): void
    {
        $container = new Container();

        $container->add('dependent-class', DependentClass::class);

        $this->assertTrue($container->has('dependent-class'));
        $this->assertFalse($container->has('foo'));
    }

    public function testServiceCanBeRecursivelyAutowired(): void
    {
        $container = new Container();

        $container->add('dependent-class', DependentClass::class);

        $dependentService = $container->get('dependent-class');

        /** @var DependencyClass $dependencyService */
        $dependencyService = $dependentService->getDependency();

        $subDependencyService = $dependencyService->getSubDependency();

        $this->assertInstanceOf(DependencyClass::class, $dependencyService);
        $this->assertInstanceOf(SubDependencyClass::class, $subDependencyService);
    }

    public function testNotInstanciatedServiceCanBeRecursivelyAutowired(): void
    {
        $container = new Container();

        $dependentService = $container->get(self::DEPENDENT_CLASS_NAME);

        $this->assertInstanceOf(DependencyClass::class, $dependentService->getDependency());
    }
}
