<?php

declare(strict_types=1);

namespace Flytomek\Framework\Tests;

class DependentClass
{
    public function __construct(private readonly DependencyClass $dependency)
    {
    }

    public function getDependency(): DependencyClass
    {
        return $this->dependency;
    }
}
