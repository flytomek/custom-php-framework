<?php

declare(strict_types=1);

namespace Flytomek\Framework\Http;

class Request
{
    /**
     * @param array<string, mixed> $getParams
     * @param array<string, mixed> $postParams
     * @param array<string, mixed> $cookies
     * @param array<string, mixed> $files
     * @param array<string, mixed> $server
     */
    final public function __construct(
        public readonly array $getParams,
        public readonly array $postParams,
        public readonly array $cookies,
        public readonly array $files,
        public readonly array $server,
    ) {
    }

    public static function createFromGlobals(): static
    {
        return new static($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
    }

    public function getRequestMethod(): string
    {
        return $this->server['REQUEST_METHOD'];
    }

    public function getPathInfo(): string
    {
        $path = explode('?', $this->server['REQUEST_URI']);
        return $path[0];
    }
}
