<?php

declare(strict_types=1);

namespace Flytomek\Framework\Http;

class HttpRequestMethodException extends HttpException
{
    protected int $statusCode = 405;
}
