<?php

declare(strict_types=1);

namespace Flytomek\Framework\Http;

class HttpNotFoundException extends HttpException
{
    protected int $statusCode = 404;
}
