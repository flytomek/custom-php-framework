<?php

declare(strict_types=1);

namespace Flytomek\Framework\Http;

final class Response
{
    public const HTTP_INTERNAL_SERVER_ERROR = 500;

    /**
     * @param string|null $content
     * @param int $status
     */
    public function __construct(
        private ?string $content = '',
        private readonly int $status = 200,
        // private array $headers = [],
    ) {
        http_response_code($this->status);
    }

    public function send(): void
    {
        echo $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }
}
