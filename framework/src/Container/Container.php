<?php

declare(strict_types=1);

namespace Flytomek\Framework\Container;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use ReflectionParameter;

class Container implements ContainerInterface
{
    /**
     * @var array<object|string>
     */
    private array $services = [];

    /**
     * @throws ContainerException
     */
    public function add(string $id, string|object $concrete = null): void
    {
        if (null === $concrete) {
            if (!class_exists($id)) {
                throw new ContainerException("Service {$id} can't be found");
            }
            $concrete = $id;
        }

        $this->services[$id] = $concrete;
    }

    /**
     * @throws NotFoundExceptionInterface
     * @throws ReflectionException
     * @throws ContainerExceptionInterface
     * @throws ContainerException
     */
    public function get(string $id)
    {
        if (!$this->has($id)) {
            if (!class_exists($id)) {
                throw new ContainerException("Service {$id} can't be found");
            }

            $this->add($id);
        }

        return $this->resolve($this->services[$id]);
    }

    /**
     * @throws NotFoundExceptionInterface
     * @throws ReflectionException
     * @throws ContainerExceptionInterface
     */
    private function resolve(object|string $class): object
    {
        if (is_string($class) && !class_exists($class)) {
            throw new ContainerException("Service {$class} can't be found");
        }

        $reflectionClass = new ReflectionClass($class);

        $constructor = $reflectionClass->getConstructor();

        if (null === $constructor) {
            return $reflectionClass->newInstance();
        }

        $constructorParams = $constructor->getParameters();

        $classDependencies = $this->resolveClassDependencies($constructorParams);

        return $reflectionClass->newInstanceArgs($classDependencies);
    }

    /**
     * @param ReflectionParameter[] $reflectionParams
     * @return object[]
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface|ReflectionException
     */
    private function resolveClassDependencies(array $reflectionParams): array
    {
        $classDependencies = [];

        foreach($reflectionParams as $param) {

            /** @var ReflectionNamedType $serviceType */
            $serviceType = $param->getType();

            $service = $this->get($serviceType->getName());

            $classDependencies[] = $service;
        }

        return $classDependencies;
    }

    public function has(string $id): bool
    {
        return array_key_exists($id, $this->services);
    }
}
