<?php

declare(strict_types=1);

namespace Flytomek\Framework\Routing;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Flytomek\Framework\Http\HttpNotFoundException;
use Flytomek\Framework\Http\HttpRequestMethodException;
use Flytomek\Framework\Http\Request;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use function FastRoute\simpleDispatcher;

class Router implements RouterInterface
{
    /**
     * @var array<string|mixed>
     */
    private array $routes;

    /**
     * @throws HttpNotFoundException
     * @throws NotFoundExceptionInterface
     * @throws HttpRequestMethodException
     * @throws ContainerExceptionInterface
     */
    public function dispatch(Request $request, ContainerInterface $container): array
    {
        $routeInfo = $this->extractRouteInfo($request);

        [$handler, $vars] = $routeInfo; // Handle status

        if(is_array($handler)) {
            [$controllerId, $action] = $handler;
            $controller= $container->get($controllerId);
            $handler = [$controller, $action];
        }

        return [$handler, $vars];
    }

    /**
     * @return array<string|mixed>
     *
     * @throws HttpNotFoundException
     * @throws HttpRequestMethodException
     */
    private function extractRouteInfo(Request $request): array
    {
        $dispatcher = simpleDispatcher(function (RouteCollector $routeCollector) {
            foreach ($this->routes as $route) {
                $routeCollector->addRoute(...$route);
            }
        });

        $routeInfo = $dispatcher->dispatch(
            $request->getRequestMethod(),
            $request->getPathInfo()
        );

        switch($routeInfo[0]) {
            case Dispatcher::FOUND:
                return [$routeInfo[1], $routeInfo[2]];
            case Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = implode(', ', $routeInfo[1]);
                throw new HttpRequestMethodException("The allowed methods are {$allowedMethods}");
            default:
                throw new HttpNotFoundException('No matching route has been found');
        }
    }

    public function setRoutes(array $routes): void
    {
        $this->routes = $routes;
    }
}
