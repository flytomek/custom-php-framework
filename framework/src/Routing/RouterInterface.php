<?php

declare(strict_types=1);

namespace Flytomek\Framework\Routing;

use Flytomek\Framework\Http\Request;
use Psr\Container\ContainerInterface;

interface RouterInterface
{
    /**
     * @param Request $request
     * @param ContainerInterface $container
     * @return array<string|mixed>
     */
    public function dispatch(Request $request, ContainerInterface $container): array;

    /**
     * @param array<string|mixed> $routes
     * @return void
     */
    public function setRoutes(array $routes): void;
}
