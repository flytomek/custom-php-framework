<?php

declare(strict_types=1);

namespace Flytomek\Framework\Console;

use DirectoryIterator;
use Flytomek\Framework\Console\Command\CommandInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use ReflectionException;

class Kernel
{

    public function __construct(
        private readonly ContainerInterface $container,
        private readonly Application $application
    ) {
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws ReflectionException
     * @throws NotFoundExceptionInterface
     */
    public function handle(): int
    {

        $this->registerCommands();

        $status = $this->application->run();

        return $status;
    }

    /**
     * @throws ReflectionException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function registerCommands(): void
    {
        $commandFiles = new DirectoryIterator(__DIR__ . '/Command');

        $namespace = $this->container->get('base-commands-namespace');

        /** @var DirectoryIterator $commandFile */
        foreach ($commandFiles as $commandFile) {
            if (!$commandFile->isFile()) {
                continue;
            }

            $commandClass = $namespace . pathinfo($commandFile->getFilename(), PATHINFO_FILENAME);

            if (is_subclass_of($commandClass, CommandInterface::class)) {
                $commandName = (new \ReflectionClass($commandClass))->getProperty('name')->getDefaultValue();

                $this->container->add($commandName, $commandClass);
            }
        }
    }
}
