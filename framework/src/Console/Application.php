<?php

declare(strict_types=1);

namespace Flytomek\Framework\Console;

use Flytomek\Framework\Console\Command\CommandInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class Application
{

    public function __construct(private readonly ContainerInterface $container)
    {
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws ConsoleException
     */
    public function run(): int
    {
        $argv = $_SERVER['argv'];

        $commandName = $argv[1] ?? null;

        if(!$commandName) {
            throw new ConsoleException('A command name must be provided');
        }

        $command = $this->container->get($commandName);

        $args = array_slice($argv, 2);

        $options = $this->parseOptions($args);

        $status = $command->execute($options);

        return $status;
    }

    private function parseOptions(array $args): array
    {
        $options = [];

        foreach($args as $arg) {
            if(str_starts_with($arg, '--')) {
                continue;
            }

            $option = explode('=', substr($arg, 2));
            $options[$option[0]] = $option[1] ?? true;

        }

        return $options;
    }
}
