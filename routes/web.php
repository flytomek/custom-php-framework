<?php

declare(strict_types=1);

use App\Controller\ArticleController;
use App\Controller\HomeController;
use Flytomek\Framework\Http\Response;

return [
    ['GET', '/', [HomeController::class, 'index']],
    ['GET', '/articles/{id:\d+}', [ArticleController::class, 'show']],
    ['GET', '/articles/create', [ArticleController::class, 'create']],
    ['GET', '/callback', function () {
        return new Response('Response from callback!');
    }]
];
