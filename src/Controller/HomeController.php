<?php

declare(strict_types=1);

namespace App\Controller;

use Flytomek\Framework\Controller\AbstractController;
use Flytomek\Framework\Http\Response;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class HomeController extends AbstractController
{

    public function __construct(private readonly Widget $widget)
    {
    }

    public function index(): Response
    {
        return $this->render('home.html.twig', [
            'name' => $this->widget->getName()
        ]);
    }
}
