<?php

declare(strict_types=1);

namespace App\Controller;

class Widget
{
    public function getName(): string
    {
        return 'Widget';
    }
}
