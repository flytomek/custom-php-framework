<?php

declare(strict_types=1);

namespace App\Controller;

use Flytomek\Framework\Controller\AbstractController;
use Flytomek\Framework\Http\Response;

class ArticleController extends AbstractController
{
    public function show(int $id): Response
    {
        return $this->render('articles/show.html.twig', [
            'id' => $id
        ]);
    }

    public function create(): Response
    {
        return $this->render('articles/create.html.twig');
    }
}
