<?php

declare(strict_types=1);
use Flytomek\Framework\Http\Kernel;
use Flytomek\Framework\Http\Request;

define('BASE_PATH', dirname(__DIR__));

require_once BASE_PATH . '/vendor/autoload.php';

/** @var League\Container\Container $container */
$container = require BASE_PATH . '/config/services.php';

$request = Request::CreateFromGlobals();

$kernel = $container->get(Kernel::class);

$response = $kernel->handle($request);

$response->send();
